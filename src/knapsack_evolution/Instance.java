package knapsack_evolution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Instance {
    
    public List<Item> items;
    public int total_cost;
    public int total_weight;
    
    public Instance(List<Item> items){
        this.items = items;
        this.total_cost = evalFitness();   
        this.total_weight = evalWeight();
    }
    
    public Instance clone(){
        List<Item> newItems = new ArrayList<Item>();
        for(Item it : items )
            newItems.add(it.clone());
            
        Instance i = new Instance(items);
        i.total_cost = this.getFitness();
        i.total_weight = this.getWeight();
        return i;
    }
    
    public int getFitness(){
        return total_cost;
    }
    
    public int getWeight(){
        return total_weight;
    }    
    
    public List<Item> getItemList(){
        return items;
    }
    
    public void setFitness(int fitness){
        this.total_cost = fitness;
    }
    
    /*
        Method which count an actual fitness of Instance.
    */
    public int evalFitness(){
        this.total_cost = 0;
        for( Item i : items )
            if( i.getPresence() )
                total_cost += i.getCost();
        
        return total_cost;
    }
    
    /*
        Method which count an total weight of Instance.
    */
    public int evalWeight(){
        this.total_weight = 0;
        for( Item i : items )
            if( i.getPresence() )
                this.total_weight += i.getWeight();
        
        return total_weight;
    }
    
    /*
        Comparator which sort an Instances by their fitness.
    */
    public static Comparator<Instance> InstanceComparator = new Comparator<Instance>() {
	public int compare(Instance i1, Instance i2) {
	   int fitness1 = i1.getFitness();
	   int fitness2 = i2.getFitness();

           /* Descending order*/
	   return fitness2 - fitness1;
    }};
    
}
