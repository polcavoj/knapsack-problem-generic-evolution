/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack_evolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author trapp
 */
public class Knapsack_Evolution {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        if(args.length != 6 ){
            System.out.println("Wrong number of arguments, usage: ");
            System.out.println("<population_size> <number_of_generations> <elite_count> <crossover_probability> <mutation_probability> <size_of_tournament>");
            return;
        }
        
        Knapsack /*knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_27.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_27.sol.dat", args);
        knapsack.solve();
        knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_30.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_30.sol.dat", args);
        knapsack.solve();
        knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_32.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_32.sol.dat", args);
        knapsack.solve();
        knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_35.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_35.sol.dat", args);
        knapsack.solve();
        knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_37.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_37.sol.dat", args);
        knapsack.solve();*//**/
        knapsack = new Knapsack("C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\inst\\knap_40.inst.dat", 
                                         "C:\\Users\\trapp\\Documents\\NetBeansProjects\\Knapsack_evolution\\sol\\knap_40.sol.dat", args);
        knapsack.solve();/**/        
    }    
}
