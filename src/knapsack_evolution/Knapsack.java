/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack_evolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author trapp
 */
public class Knapsack {
    
    private File file;
    private File solution_file;
    BufferedReader br_file;
    BufferedReader br_sol;
    private List<Item> items;
    private int items_count;
    private int items_max_weight;
    private int real_best_cost;
    private int evaluated_best_cost;
    private int population_limit;
    private int elite_count;
    private int generations;
    private double crossover_probability;
    private double mutation_probability;
    private int tournament_size;
    private List<Instance> population;
    private List<Instance> newPopulation;
    private long start_time;
    private long end_time;
    private long overall_time;
    private double worst_deviation;
    private double relative_deviation;
    
    Random rand;
    
    /*
        items - List of initial items that could be in the knapsack.
        items_count - Number of items.
        items_max_weight - Max weight of items in the knapsack.
        real_best_cost - Best cost which is solved by bruteforce.
        population_limit - First argument - limit of population in the evolution algorithm.
        generations - Second argument - number of generations of the evolution.
        elite_count - Third argument - number of elite Instances, which goes right to the next generation.
        crossover_probability - Third argument - decimal number of probability to crossover an Instance.
        mutation_probability - Fourth argument - decimal number with probability of inverting each item.
        tournament_size - Fifth argument - size of the tournament for choosing a parents.
        population - List of actual population.
        newPopulation - List of next population.
        rand - Random generator.
        start_time - Start time of the algorithm.
        end_time - end time of the algorithm.
        overall_time - overall computation time of all instances in one file.
        worst_deviation - Worst deviation of all instances in one file.
        relative_deviation - Relative deviation of all instances in one file.
    */
    Knapsack(String input_file, String solution_file, String [] args) throws FileNotFoundException {
        openFile(input_file, solution_file);
        this.items = new ArrayList<Item>();
        this.items_count = 0;
        this.items_max_weight = 0;
        this.real_best_cost = 0;
        this.evaluated_best_cost = 0;
        this.population_limit = Integer.parseInt(args[0]);
        this.generations = Integer.parseInt(args[1]);
        this.elite_count = Integer.parseInt(args[2]);
        this.crossover_probability = Double.parseDouble(args[3]);
        this.mutation_probability = Double.parseDouble(args[4]);
        this.tournament_size = Integer.parseInt(args[5]);
        this.population = new ArrayList<Instance>();
        this.newPopulation = new ArrayList<Instance>();
        this.rand = new Random(); 
        this.start_time = 0;
        this.end_time = 0;
        this.overall_time = 0;
    }
    
    /*
        Method for opening the input file.
    */
    public void openFile(String input_file, String solution_file) throws FileNotFoundException{
        this.file = new File(input_file);
        this.solution_file = new File(solution_file);
        this.br_file = new BufferedReader(new FileReader(this.file));
        this.br_sol = new BufferedReader(new FileReader(this.solution_file));
        System.out.println("Reading from a file: " + this.file.getName() + " and: " + this.solution_file.getName());
    }

    /*
        Main solving function fo knapsack. It goes through all knapsack instances in the file and solving them.
    */
    void solve() throws IOException {
        String input_line = "", solution_line = "";
        double actual_deviation = 0;
        while ((input_line = br_file.readLine()) != null){
            this.start_time = System.currentTimeMillis();
            clearResults();
            parseInput(input_line.split(" "));
            if( (solution_line = br_sol.readLine()) != null)
                parseSolutionInput(solution_line.split(" "));
            
            generateInitialPopulation();
            
            Collections.sort(population, Instance.InstanceComparator);
            
            evolution();
            
            this.end_time = System.currentTimeMillis();
            getBestInstance();
            this.overall_time += this.end_time - this.start_time;
            
            actual_deviation = (((double)this.real_best_cost - (double)this.evaluated_best_cost) / (double)this.real_best_cost);
            
            relative_deviation += actual_deviation;
            if( actual_deviation > worst_deviation )
                worst_deviation = actual_deviation;
        } 
        getMeasureResults();
    }

    /*
        Clearing results for another knapsack instance.    
    */
    private void clearResults() {
        this.items.clear();
        this.items_count = 0;
        this.items_max_weight = 0;
        this.population.clear();
        this.newPopulation.clear();
    }

    /*
        Parsing an input line to the variables.
    */
    private void parseInput(String [] input_array) {
        items_count = Integer.parseInt(input_array[1]); //number of items
        items_max_weight = Integer.parseInt(input_array[2]); //max weight of the knapsack
        
        for(int i = 3; i < 2*items_count + 3; i = i + 2){
            items.add(new Item(Integer.parseInt(input_array[i]), Integer.parseInt(input_array[i+1]), false));
        }   
    }
    
    /*
        Parsing a real cost of the best solution.
    */
    private void parseSolutionInput(String[] input_array) {
        this.real_best_cost = Integer.parseInt(input_array[2]);
    }

    
    /*
        Generating an initial population.
    */
    private void generateInitialPopulation() {
        for( int i = 0; i < this.population_limit; i++ ){
            List <Item> newItems = new ArrayList<Item>();
            for( int j = 0; j < this.items.size(); j++){
                Item newItem = (new Item(items.get(j).getWeight(), items.get(j).getCost(), false));
                
                int value = rand.nextInt(20);
                if( value == 0 )
                    newItem.setPresence(true);
                
                newItems.add(newItem);                
            }
            Instance newPop = new Instance(newItems);
            
            if( newPop.getWeight() > this.items_max_weight )
                newPop.setFitness(0);
            
            this.population.add(newPop);
        }        
    }    

    /*
        Main evolution method - it goes through the # of generations and create a new populations.
    */
    private void evolution() {
        for(int i = 0; i < this.generations; i++ ){
            cloneElites();
            
            /*System.out.print("Generation: " + i + " - ");
            for(int j = 0; j < this.population_limit - 1; j++ )
                System.out.print(this.population.get(j).getFitness() + " ");
            System.out.println();*/
            
            double avg = 0;
            for( int j = 0; j < this.population_limit - 1; j++ ){
                avg += this.population.get(j).getFitness();
            }
            avg = avg / this.population_limit;
            System.out.println(this.population.get(0).getFitness() + " " + avg);
            
            List<Instance> parents = chooseParents();
            
            createsCrossovers(parents);
            
            mutate();
            
            doEvolutionStep();
        }
    }
    
    /*
        Method which clone a number of elites based on the argument to the next generation.
    */
    private void cloneElites(){
        for(int i = 0; i < this.elite_count; i++ ){
            newPopulation.add(this.population.get(i).clone());
        }
    }
    
    /*
        Method which do evolution step to the new generation.
    */
    private void doEvolutionStep(){
        population.clear();
        for( Instance i : newPopulation )
            population.add(i.clone());        
        newPopulation.clear();
        recalculateResults();
        Collections.sort(population, Instance.InstanceComparator); 
    }
    
    /*
        Method which recalculate fitness of population.
    */
    private void recalculateResults(){
        for( int i = 0; i < this.population_limit - 1; i++ ){
            if( this.population.get(i).evalWeight() > this.items_max_weight )
                this.population.get(i).setFitness(0);
            else
                this.population.get(i).evalFitness();
        }
    }

    /*
        Function which write best solution to the console.
    */
    private void getBestInstance() {
        //System.out.println("Best fitness is: " + this.population.get(0).getFitness());
        this.evaluated_best_cost = this.population.get(0).getFitness();
        /*for(int i = 0; i < this.items_count; i++ ){
            if( this.population.get(0).items.get(i).getPresence() )
                System.out.print("1 ");
            else System.out.print("0 ");
        }
        System.out.print("\n");*/
    }

    /*
        Choosing a parents to the next generation with tournament function.
    */
    private List<Instance> chooseParents() {
        List<Instance> parents = new ArrayList<Instance>();
        Instance tournamentWinner;
        for( int i = 0; i < this.population_limit - this.elite_count; i++ ){
            tournamentWinner = tournament();
            parents.add(tournamentWinner);
        }
        
        return parents;
    }
    
    /*
        Tournament function which select the best of the # of instances.
    */
    private Instance tournament(){
        List<Instance> tournamentInstances = new ArrayList<Instance>();
        int value = 0;
        int count = 0;
        while(count != this.tournament_size){
            value = rand.nextInt(49 - this.elite_count) + 1;
            tournamentInstances.add(this.population.get(value));
            count++;
        }
        
        Collections.sort(tournamentInstances, Instance.InstanceComparator);
        
        return tournamentInstances.get(0);        
    }

    /*
        Function which pair crossovers and call a cross function.
    */
    private void createsCrossovers(List<Instance> parents) {
        double value = 0;
        for( int i = 0; i < this.population_limit - 1 - elite_count; i = i + 2 ){
            value = rand.nextDouble();
            if( value <= this.crossover_probability ) {
                cross(parents.get(i), parents.get(i+1));
            }
            else{
                newPopulation.add(parents.get(i).clone());
                newPopulation.add(parents.get(i+1).clone());
            }
        }
        
        newPopulation.add(parents.get(this.population_limit - 1 - elite_count).clone());
     
    }

    /*
        Crossing function, which combinate two instances together and create a new one.
    */
    private void cross(Instance first, Instance second) {
        List<Item> crossedItems = new ArrayList<Item>();
        List<Item> crossedItemsSecond = new ArrayList<Item>();
        int value = rand.nextInt(this.items_count - 1);
        for( int i = 0; i < value; i++ ){
            crossedItems.add(first.getItemList().get(i).clone());
            crossedItemsSecond.add(second.getItemList().get(i).clone());
        }
        
        for( int i = value; i < this.items_count; i++ ){
            crossedItems.add(second.getItemList().get(i).clone());
            crossedItemsSecond.add(first.getItemList().get(i).clone());
        }
        
        Instance crossedInstance = new Instance(crossedItems);
        Instance crossedInstanceSecond = new Instance(crossedItemsSecond);

        this.newPopulation.add(crossedInstance);
        if( this.newPopulation.size() != this.population_limit )
            this.newPopulation.add(crossedInstanceSecond);
    }

    /*
        Mutation over all instances in new generation and all of their items.
    */
    private void mutate() {
        double value;
        
        for( int i = this.elite_count; i < this.population_limit - 1; i++ ){
            for( int j = 0; j < this.items_count; j++ ){
                value = rand.nextDouble();
                if( value <= this.mutation_probability ){
                    if( this.newPopulation.get(i).items.get(j).getPresence() )
                        this.newPopulation.get(i).items.get(j).setPresence(false);
                    else
                        this.newPopulation.get(i).items.get(j).setPresence(true);
                }
            }                
        }
    }

    private void getMeasureResults() {
        System.out.println("EA time: " + ((double)this.overall_time / 50));
        System.out.println("Relative deviation: " + (this.relative_deviation / 50));
        System.out.println("Worst deviation: " + this.worst_deviation);
    }
}
